#include <gtkmm.h>
#include "version.h"

class HelloWorld : public Gtk::Window {
        public:
                HelloWorld();
                ~HelloWorld();
        private:
                Glib::RefPtr<Gtk::Builder> hello_world;
                Glib::RefPtr<Gdk::Pixbuf> icon;

                void on_action_about();
};

HelloWorld::HelloWorld()
{
        hello_world = Gtk::Builder::create_from_resource("/org/gtkmm/Linux_a_H/ui/hello-world.glade");
        icon = Gdk::Pixbuf::create_from_resource("/org/gtkmm/Linux_a_H/icons/hello-world.svg");

        Gtk::HeaderBar *header_bar = nullptr;
        hello_world->get_widget("header_bar", header_bar);
        Gtk::MenuButton *menu_button = nullptr;
        hello_world->get_widget("menu_button", menu_button);
        Glib::RefPtr<Gio::Menu> menu = Gio::Menu::create();
        menu->append("About", "hello-world.about");
        menu_button->set_menu_model(menu);
        auto menu_action = Gio::SimpleActionGroup::create();
        menu_action->add_action("about", sigc::mem_fun(
                                *this, &HelloWorld::on_action_about));

        Gtk::Label *label = nullptr;
        hello_world->get_widget("label", label);

        set_titlebar(*header_bar);
        insert_action_group("hello-world", menu_action);
        add(*label);
}

HelloWorld::~HelloWorld()
{
}

void
HelloWorld::on_action_about()
{
        Gtk::AboutDialog *about_dialog = nullptr;
        hello_world->get_widget("about_dialog", about_dialog);

        about_dialog->set_transient_for(*this);
        about_dialog->set_modal();

        Glib::ustring version = "";
        version += Glib::ustring::format(VERSION_MAJOR);
        version += ".";
        version += Glib::ustring::format(VERSION_MINOR);

        about_dialog->set_version(version);
        about_dialog->set_logo(icon);
        about_dialog->show();
}

static void
on_activate(Glib::RefPtr<Gtk::Application> app)
{
        Gtk::Window *window = app->get_active_window();

        if (not window) {
                window = new HelloWorld();
                window->property_application() = app;
                window->property_default_width() = 520;
                window->property_default_height() = 240;
                app->add_window(*window);
        }

        window->present();
}

int
main()
{
        Glib::RefPtr<Gtk::Application> app = Gtk::Application::create("org.gtkmm.Linux_a_H.hello-world");

        app->signal_activate().connect(sigc::bind(&on_activate, app));

        return app->run();
}
